# README #

Here you can read all necessary steps to get a picture wall with a modal bootstrap dialog on your site.


### Description ###

* A picture / portrait wall for news articles with a modal bootstrap dialog showing more information about the pictures / people
* 0.1

### How do I get set up? ###

* Download everything
* Run bower init
* Run grunt or grunt dist
* Then you should have everything in one folder called generated and the uglified version in dist

### Contribution guidelines ###

* Code review
* Check functionality

### Who do I talk to? ###

* Felix Michel, contact me through bitbucket